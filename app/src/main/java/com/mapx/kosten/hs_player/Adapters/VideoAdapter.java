package com.mapx.kosten.hs_player.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapx.kosten.hs_player.CustomItemClickListener;
import com.mapx.kosten.hs_player.R;
import com.mapx.kosten.hs_player.Objects.Video;

import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder>{
    private static final String TAG = "AdapterVideo";

    private List<Video> mVideoList;
    private CustomItemClickListener mClickListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView mNameVideo;
        TextView mUrlVideo;

        ViewHolder(View itemView) {
            super(itemView);
            mNameVideo = (TextView)itemView.findViewById(R.id.textView_name);
            mUrlVideo = (TextView)itemView.findViewById(R.id.textView_url);

            itemView.setOnClickListener(new CardOnClickListener());
        }

        class CardOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) mClickListener.onItemClick(itemView, getAdapterPosition());
            }
        }
    }

    public void setClickListener(CustomItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public VideoAdapter(List<Video> videoList){
        mVideoList = videoList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                      int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_video, viewGroup, false);
        VideoAdapter.ViewHolder pvh = new VideoAdapter.ViewHolder(v);
        return pvh;
    }
    public void updateDataset(List<Video> contents) {
        if (mVideoList == null)
            mVideoList = new ArrayList<>();
        mVideoList.clear();
        mVideoList.addAll(contents);
        notifyDataSetChanged();
    }

    public Video getVideoAtIndex(int index) {
        return mVideoList.get(index);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(VideoAdapter.ViewHolder holder, int i) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mNameVideo.setText(mVideoList.get(i).getName());
        holder.mUrlVideo.setText(mVideoList.get(i).getUrl());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mVideoList.size();
    }

}
