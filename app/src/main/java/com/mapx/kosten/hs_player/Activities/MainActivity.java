package com.mapx.kosten.hs_player.Activities;

import android.content.Intent;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mapx.kosten.hs_player.CustomItemClickListener;
import com.mapx.kosten.hs_player.Utils.PlayerUtil;
import com.mapx.kosten.hs_player.R;
import com.mapx.kosten.hs_player.Objects.Video;
import com.mapx.kosten.hs_player.Adapters.VideoAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

import static com.mapx.kosten.hs_player.Utils.PlayerUtil.LIST_NAME_KEY;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.LIST_URL_KEY;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.TOAST_DURATION;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.clearArrayList;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.getArrayList;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.saveArrayList;


public class MainActivity extends AppCompatActivity implements CustomItemClickListener {

    private static boolean mLoadDefaultList = true;

    private Toolbar mToolbar;
    private FabSpeedDial mFabDial;
    private String[] mVideoName;
    private String[] mVideoUri;
    private VideoAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutMgr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbarLayout_main);
        mFabDial = (FabSpeedDial) findViewById(R.id.fab_speed_dial);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_video);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");

        // linear layout manager & recyclerView
        mLinearLayoutMgr = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutMgr);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // load name and uri list
        loadVideoList(mLoadDefaultList);
        mLoadDefaultList = false;

        // add listener to delete items
        deleteVideo();

        mFabDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_addList:
                        Intent intentVideo =
                                new Intent(MainActivity.this, AddPlaylistActivity.class);
                        startActivity(intentVideo);
                        break;
                    case R.id.action_addVideo:
                        Intent intentList =
                                new Intent(MainActivity.this, AddVideoActivity.class);
                        startActivity(intentList);
                        break;
                    case R.id.action_clearList:
                        clearPlaylist();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        String[] videoUrls, extension;
        Intent intent;
        int i, pos;

        // build playlist
        extension = new String[mVideoUri.length];
        videoUrls = new String[mVideoUri.length];
        for (i = 0; i < mVideoUri.length; i++) {
            pos = i + position;
            if ((i+position) < mVideoUri.length) {
                videoUrls[i] = mVideoUri[pos];
            } else {
                videoUrls[i] = mVideoUri[pos - mVideoUri.length];
            }
            extension[i] = PlayerUtil.EXTENSION_FILENAME;
        }

        intent = new Intent(MainActivity.this, PlayerActivity.class);
        intent.putExtra(PlayerActivity.PREFER_EXTENSION_DECODERS, false);
        intent.setAction(PlayerActivity.ACTION_VIEW_LIST);
        intent.putExtra(PlayerActivity.URI_LIST_EXTRA, videoUrls);
        intent.putExtra(PlayerActivity.EXTENSION_LIST_EXTRA, extension);

        // launch player
        startActivity(intent);
    }

    // load playlist
    public void loadVideoList( boolean load_defautl_list){
        ArrayList<String> mNameList = null;
        ArrayList<String> mUriList = null;
        ArrayList<Video> mVideoList = new ArrayList<>();
        Video itemVideo;

        // get list from shared preferences
        mNameList = getArrayList(LIST_NAME_KEY, this);
        mUriList = getArrayList(LIST_URL_KEY, this);

        if (mNameList != null) {
            mVideoName = new String[mNameList.size()];
            mVideoName = mNameList.toArray(mVideoName);
        }

        if (mUriList != null) {
            mVideoUri = new String[mUriList.size()];
            mVideoUri = mUriList.toArray(mVideoUri);
        }

        if ((mVideoName == null || mVideoUri == null) &&
                load_defautl_list) {

            // load default playlist
            Resources res = getResources();
            mVideoName = res.getStringArray(R.array.video_array);
            mVideoUri = res.getStringArray(R.array.url_array);

            // save list in shared preferences
            mNameList = new  ArrayList<String>(Arrays.asList(mVideoName));
            saveArrayList(mNameList, LIST_NAME_KEY, this);
            mUriList = new  ArrayList<String>(Arrays.asList(mVideoUri));
            saveArrayList(mUriList, LIST_URL_KEY, this);

            // show msg
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Load Default Playlist", TOAST_DURATION);
            toast.show();
        }

        if (mVideoName != null && mVideoUri != null) {
            for (int i=0; i<mNameList.size(); i++) {
                itemVideo = new Video(mNameList.get(i), mUriList.get(i));
                mVideoList.add(itemVideo);
            }
        }

        // set list to adapter
        mAdapter = new VideoAdapter(mVideoList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);

    }


    // Delete video handler
    private void deleteVideo() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView,
                                  RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                ArrayList<String> mName = null;
                ArrayList<String> mUri = null;

                // get list from shared preferences
                mName = getArrayList(LIST_NAME_KEY, getApplicationContext());
                mUri = getArrayList(LIST_URL_KEY, getApplicationContext());

                // delete video item
                mName.remove(viewHolder.getAdapterPosition());
                mUri.remove(viewHolder.getAdapterPosition());

                // save array list in to shared preferences
                saveArrayList(mName, LIST_NAME_KEY, getApplicationContext());
                saveArrayList(mUri, LIST_URL_KEY, getApplicationContext());

                // update adapter
                loadVideoList(false);
            }

            @Override
            public void onMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                int fromPos, RecyclerView.ViewHolder target,
                                int toPos, int x, int y) {
                super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    // Clear list handler
    private void clearPlaylist() {

        // clear list view
        mVideoName=null;
        mVideoUri=null;

        // clear list in shared preferences
        clearArrayList(getApplicationContext());

        // update adapter
        loadVideoList(false);
    }

    // Create menu on toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Listener items of menues
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.item_settings:
                Intent intent_settings = new Intent(this, SettingsActivity.class);
                startActivity(intent_settings);
                return true;
            case R.id.item_about:
                Intent intent_about = new Intent(this, AboutActivity.class);
                startActivity(intent_about);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadVideoList(false);
    }
}
