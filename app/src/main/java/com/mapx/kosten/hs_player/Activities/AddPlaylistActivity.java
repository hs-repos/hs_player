package com.mapx.kosten.hs_player.Activities;

import com.mapx.kosten.hs_player.Objects.Video;
import com.mapx.kosten.hs_player.R;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Xml;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.mapx.kosten.hs_player.Utils.PlayerUtil.TOAST_DURATION;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.updatePlayList;

public class AddPlaylistActivity extends AppCompatActivity {

    private static final int READ_REQUEST_CODE = 42;
    private static final String ns = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_playlist);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    // Add list button action
    public void onBtnAddListClick(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(Intent.createChooser(intent, "Select a playlist"), READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        List playlistList = null;

        if(requestCode==READ_REQUEST_CODE && resultCode==RESULT_OK) {
            if (resultData != null) {
                Uri selectedfile = resultData.getData();

                try {
                    playlistList  = readTextFromUri(selectedfile);
                    // show msg
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Video> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List readTextFromUri(Uri uri) throws IOException {
        ArrayList<Video> videoList = new ArrayList<>();
        Toast toast;
        InputStream inputStream = null;
        inputStream = getContentResolver().openInputStream(uri);

        try {
            videoList = parse(inputStream);
            updatePlayList(videoList, this);
            toast = Toast.makeText(getApplicationContext(), "Playlist added...", TOAST_DURATION);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            toast = Toast.makeText(getApplicationContext(), "Playlist error!", TOAST_DURATION);
        }

        toast.show();
        inputStream.close();

        return videoList;
    }

    private ArrayList<Video> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Video> entries = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "playlist");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("video")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    private Video readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "video");
        String name = null;
        String url = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String label = parser.getName();
            if (label.equals("name")) {
                name = readName(parser);
            } else if (label.equals("url")) {
                url = readUrl(parser);
            } else {
                skip(parser);
            }
        }
        return new Video(name, url);
    }

    // Processes title tags in the feed.
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return title;
    }

    // Processes title tags in the feed.
    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "url");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "url");
        return title;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
