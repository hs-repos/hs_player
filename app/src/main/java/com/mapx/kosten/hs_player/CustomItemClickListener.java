package com.mapx.kosten.hs_player;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
