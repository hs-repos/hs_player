package com.mapx.kosten.hs_player.Activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mapx.kosten.hs_player.R;
import com.mapx.kosten.hs_player.Utils.PlayerUtil;

import static com.mapx.kosten.hs_player.Utils.PlayerUtil.KEY_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.KEY_BUFFER_FOR_PLAYBACK_MS;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.KEY_MAX_BUFFER_MS;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.KEY_MIN_BUFFER_MS;
import static com.mapx.kosten.hs_player.Utils.PlayerUtil.PREF_FILENAME;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText mEditTestMinBuffer;
    private EditText mEditTestMaxBuffer;
    private EditText mEditTestPlaybackBuffer;
    private EditText mEditTestRePlaybackBuffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mToolbar = (Toolbar) findViewById(R.id.toolbarLayout_main);
        mEditTestMinBuffer = (EditText) findViewById(R.id.edittext_minBuffer);
        mEditTestMaxBuffer = (EditText) findViewById(R.id.edittext_maxBuffer);
        mEditTestPlaybackBuffer = (EditText) findViewById(R.id.edittext_playBuffer);
        mEditTestRePlaybackBuffer = (EditText) findViewById(R.id.edittext_playReBuffer);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSettings();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Cancel button, return to main activity
    public void onBtnSaveConfigClick(View view) {
        Toast toast;
        int duration = Toast.LENGTH_SHORT;
        savePreferences();
        toast = Toast.makeText(getApplicationContext(), "Preferences saved...", duration);
        toast.show();
    }

    public void getSettings() {
        SharedPreferences pref = getSharedPreferences(PREF_FILENAME, MODE_PRIVATE);
        String min_buffer = pref.getString(KEY_MIN_BUFFER_MS, PlayerUtil.MIN_BUFFER_MS);
        String max_buffer = pref.getString(KEY_MAX_BUFFER_MS, PlayerUtil.MAX_BUFFER_MS);
        String playback_buffer = pref.getString(KEY_BUFFER_FOR_PLAYBACK_MS, PlayerUtil.BUFFER_FOR_PLAYBACK_MS);
        String replayback_buffer = pref.getString(KEY_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS, PlayerUtil.BUFFER_FOR_PLAYBACK_AFTER_MS);

        // update edit text
        mEditTestMinBuffer.setText(min_buffer);
        mEditTestMaxBuffer.setText(max_buffer);
        mEditTestPlaybackBuffer.setText(playback_buffer);
        mEditTestRePlaybackBuffer.setText(replayback_buffer);

    }

    private void savePreferences() {
        SharedPreferences pref = getSharedPreferences(PREF_FILENAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        String min_buffer = mEditTestMinBuffer.getText().toString();
        String max_buffer = mEditTestMaxBuffer.getText().toString();
        String playback_buffer = mEditTestPlaybackBuffer.getText().toString();
        String replayback_buffer = mEditTestRePlaybackBuffer.getText().toString();

        editor.putString(KEY_MIN_BUFFER_MS, min_buffer);
        editor.putString(KEY_MAX_BUFFER_MS, max_buffer);
        editor.putString(KEY_BUFFER_FOR_PLAYBACK_MS, playback_buffer);
        editor.putString(KEY_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS, replayback_buffer);

        editor.commit();
    }
}
