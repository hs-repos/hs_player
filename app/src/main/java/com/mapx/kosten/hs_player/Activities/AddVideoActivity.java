package com.mapx.kosten.hs_player.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mapx.kosten.hs_player.R;
import com.mapx.kosten.hs_player.Objects.Video;

import java.util.ArrayList;

import static com.mapx.kosten.hs_player.Utils.PlayerUtil.updatePlayList;


public class AddVideoActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText mEditTxtName;
    private EditText mEditTxtUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);

        mToolbar = (Toolbar) findViewById(R.id.toolbarLayout_main);
        mEditTxtName = (EditText) findViewById(R.id.edittext_name);
        mEditTxtUri = (EditText) findViewById(R.id.edittext_uri);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // OK button, save entry and return to main activity
    public void onBtnAddOkClick(View view) {

        if (mEditTxtName.getText().toString().trim().isEmpty() ||
                mEditTxtUri.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Name and Url are required!", Toast.LENGTH_SHORT).show();
        } else {
            // create array list
            ArrayList<Video> videoList = new ArrayList<>();
            // create new video object
            Video videoItem = new Video("","");

            // insert new entry
            videoItem.setName(mEditTxtName.getText().toString());
            videoItem.setUrl(mEditTxtUri.getText().toString());
            videoList.add(videoItem);
            updatePlayList(videoList, this);

            // return to main
            finish();
        }

    }

    // Cancel button, return to main activity
    public void onBtnAddCancelClick(View view) {
        finish();
    }

}
