package com.mapx.kosten.hs_player.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapx.kosten.hs_player.Objects.Video;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.accounts.AccountManager.KEY_PASSWORD;
import static android.content.Context.MODE_PRIVATE;

public final class PlayerUtil {

    public static final int TOAST_DURATION = Toast.LENGTH_SHORT;

    // video extension
    public final static String EXTENSION_FILENAME = "m3u8";

    // keys of the shared preferences configuration
    public final static String PREF_FILENAME = "playerConfig";
    public final static String KEY_MIN_BUFFER_MS = "min_buffer";
    public final static String KEY_MAX_BUFFER_MS = "max_buffer";
    public final static String KEY_BUFFER_FOR_PLAYBACK_MS = "buffer_playback";
    public final static String KEY_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS = "buffer_re_playback";

    // default values of the buffers configuration
    public final static String MIN_BUFFER_MS = "1000";
    public final static String MAX_BUFFER_MS = "1000";
    public final static String BUFFER_FOR_PLAYBACK_MS = "1000";
    public final static String BUFFER_FOR_PLAYBACK_AFTER_MS = "1000";

    // handler to save playlist in shared preferences
    public static void saveArrayList(ArrayList<String> list, String key, Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }

    // handler to get playlist from shared preferences
    public static ArrayList<String> getArrayList(String key, Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    // handler to clear playlist in shared preferences
    public static void clearArrayList(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

    // keys of the shared preferences playlist
    public final static String LIST_NAME_KEY = "Name";
    public final static String LIST_URL_KEY = "Url";

    // read the playlist from shared preferences, add the new entries and save the playlist
    public static void updatePlayList(ArrayList<Video> videos, Context context) {
        ArrayList<String> mName = null;
        ArrayList<String> mUri = null;
        // get array list
        mName = PlayerUtil.getArrayList(PlayerUtil.LIST_NAME_KEY, context);
        if (mName == null) {
            mName = new ArrayList<String>();
        }

        mUri = PlayerUtil.getArrayList(PlayerUtil.LIST_URL_KEY, context);
        if (mUri == null) {
            mUri = new ArrayList<String>();
        }

        for (int i=0; i<videos.size() ; i++){
            // insert new entry
            mName.add(videos.get(i).getName());
            mUri.add(videos.get(i).getUrl());
        }

        // save array list
        PlayerUtil.saveArrayList(mName, PlayerUtil.LIST_NAME_KEY, context);
        PlayerUtil.saveArrayList(mUri, PlayerUtil.LIST_URL_KEY, context);
    }

}
